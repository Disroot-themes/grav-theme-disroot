# Disroot Fresh Theme

![Disroot](assets/disroot_screenshot.png)

The **Disroot** Theme is for [Grav CMS](http://github.com/getgrav/grav).  This README.md file should be modified to describe the features, installation, configuration, and general usage of this theme.

## Description

The Disroot theme was created for disroot.org website.

# Features:

* HTML5 and CSS3
* No JavaScript
* Fully Responsive
* Clean and modern design
* Fontawesome icon font
* Local custom web fonts
* Cross browser compatible

# Installation

As for now the disroot theme is not released via GPM and only manual install is possible.

# Manual Installation

To install this theme, just download the zip version of this repository and unzip it under /your/site/grav/user/themes. Then, rename the folder to disroot.

You should now have all the theme files under
`/your/site/grav/user/themes/disroot`

### Tweaking the theme when using vagrant
When you use the Website vagrant deployment and work on the Theme templates, you sometimes have to clear the cache in order to see your change on the website.
The simplest way to do this is by going to the root Grav directory in Terminal and typing bin/grav clearcache.
